class MyWidget(QtGui.QWidget):
    def __init__(self)
        # Simple way of setting the background pattern at initialization
        palette = QtGui.QPalette()
        brush = QtGui.QBrush( QtGui.QColor(255, 50, 0), QtCore.Qt.DiagCrossPattern)
        palette.setBrush(self.backgroundRole(), brush)
        self.setPalette(palette)
        self.setAutoFillBackground(True)
    
    def paintEvent(self, event):
        # Mora complicated method, but more flexible as well.
        """source: https://stackoverflow.com/questions/41031416/painting-background-on-qgraphicsview-using-drawbackground"""
        # Set Pattern
        painter = QtGui.QPainter(self)
        brush = QtGui.QBrush( QtGui.QColor(255, 0, 0), QtCore.Qt.BDiagPattern)# QtGui.QBrush()
        painter.setBrush(brush)
        pen = QtGui.QPen()
        penWidth = 0
        pen.setWidth(0)
        painter.setPen(pen)
        rect = [-penWidth, -penWidth, self.width()+penWidth, self.height()+penWidth]
        painter.drawRect(rect[0], rect[1], rect[2], rect[3])