import os
import sys

def main():
    startDir = sys.argv[1]
    if not os.path.exists(startDir):
        print "No such file or directory '%s'"%(startDir)
        return -1
    for root, folders, files in os.walk(startDir):
        if len(files) == 0 and len(folders) == 0:
            print root
            continue


if __name__ == "__main__":
    run = main()
    if run == "-1":
        sys.exit(-1)
    else:
        sys.exit(1)